# Java Buildpack Image

This docker image can be used to build the [Java buildpack](https://github.com/cloudfoundry/java-buildpack).

The docker image is originally taken from here: https://github.com/cloudfoundry/java-buildpack/blob/master/ci/docker-image/Dockerfile

